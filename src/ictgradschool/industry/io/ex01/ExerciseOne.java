package ictgradschool.industry.io.ex01;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class ExerciseOne {

    public void start() {

        printNumEsWithFileReader();

        printNumEsWithBufferedReader();

    }

    private void printNumEsWithFileReader() {

        int numE = 0;
        int total = 0;
        int randomInt = 0;
//        FileReader fR = new FileReader("H:/ICT/Lab14/2018s_industry_lab_08/input2.txt");
        try (FileReader fR = new FileReader("input2.txt")) {
            while ((randomInt = fR.read())!= -1){
                total++;
                randomInt = (char)randomInt;
                if (randomInt == 'e'|| randomInt== 'E'){
                    numE++;
                }
            }

        }catch (IOException e){
            System.out.println("File not found" + e);
        }
        // TODO Read input2.txt and print the total number of characters, and the number of e and E characters.
        // TODO Use a FileReader.

        System.out.println("Number of e/E's: " + numE + " out of " + total);
    }

    private void printNumEsWithBufferedReader() {

        int numE = 0;
        int total = 0;
        int s = 0;
        try (BufferedReader bR =  new BufferedReader(new FileReader("input2.txt"))){
        while ((s = bR.read()) !=-1){
            total ++;
            s = (char)s;
            if (s == 'e'|| s== 'E'){
                numE++;
            }
            }
        }catch (IOException e){
            System.out.println("File not found" + e);
        }

        // TODO Read input2.txt and print the total number of characters, and the number of e and E characters.
        // TODO Use a BufferedReader.

        System.out.println("Number of e/E's: " + numE + " out of " + total);
    }

    public static void main(String[] args) {
        new ExerciseOne().start();
    }

}
