package ictgradschool.industry.io.ex05;
import ictgradschool.Keyboard;
import ictgradschool.industry.io.ex05.User;

import java.io.*;
import java.util.Scanner;

public class GameMaster {
    private final int TOTAL_ATTEMPTS = 7;
    private String[] theArray;
    private String mOrA;
    public void start() {
        PrintHelper.saveLine("Would you like to play automatically or manually?");
        mOrA = PrintHelper.keyboardSave();
        if (mOrA.equals("manually")) {
            theStart();
        } else if (mOrA.equals("automatically")) {
            PrintHelper.saveLine("Please enter a file name: ");
            String fileName = PrintHelper.keyboardSave() + ".txt";
            while (!fileName.equals("input5.txt")) {
                PrintHelper.saveLine("Not Valid.Please enter a file name: ");
                fileName = PrintHelper.keyboardSave() + ".txt";
            }
            try (Scanner scanner = new Scanner(new FileReader(fileName))) {
                int i = 0;
                theArray = new String[7];
                while (scanner.hasNextLine()) {
                    theArray[i] = scanner.nextLine();
                    i++;
                }
            } catch (IOException e) {
                PrintHelper.saveLine("File not found");
            }
        } else {
            PrintHelper.saveLine("Fail");
            start();
        }
        theStart();
    }

    public void theStart() {
        PrintHelper.saveLine("Please enter your secret code below:");
        User user = new User();
        Computer computer = new Computer();
        String secretCode = user.getSecretCodeOrGuess();
        String computerSecretCode = computer.getComputerCode();
        theGame(secretCode, computerSecretCode);
    }

    public void theGame(String secretCode, String computerSecretCode) {
        String userGuess = "";
        String result = "Neither a win or a loss...";
        int bullCounterG, bullCounter;
        int cowCounterG, cowCounter;
        int counter = 0;
        outerloop:
        while (counter < TOTAL_ATTEMPTS) {
            PrintHelper.saveLine("---");
            if (mOrA.equals("manually")){
                PrintHelper.saveLine("Your guess: ");
                User user = new User();
                userGuess = user.getSecretCodeOrGuess();
            } else if (mOrA.equals("automatically")) {
                PrintHelper.saveLine("Your guess: " + theArray[counter]);
                userGuess = theArray[counter];
            }

                Computer computer = new Computer();
                String computerGuess = computer.getComputerCode();
                bullCounter = 0;
                cowCounter = 0;
                bullCounterG = 0;
                cowCounterG = 0;
                for (int codeCounter = 0; codeCounter < 4; codeCounter++) {
                    char c = userGuess.charAt(codeCounter);
                    if (c == computerSecretCode.charAt(codeCounter)) {
                        bullCounter += 1;
                    } else if (computerSecretCode.indexOf(c) != -1) {
                        cowCounter += 1;
                    }
                }
                PrintHelper.saveLine("Result: " + bullCounter + " bull and " + cowCounter + " cows");
                if (userGuess.equals(computerSecretCode)) {
                    result = "You Win! :)";
                    break outerloop;
                }
                for (int computerCounter = 0; computerCounter < 4; computerCounter++) {
                    char d = computerGuess.charAt(computerCounter);
                    if (d == secretCode.charAt(computerCounter)) {
                        bullCounterG += 1;
                    } else if (secretCode.indexOf(d) != -1) {
                        cowCounterG += 1;
                    }
                }
                PrintHelper.saveLine("Computer guess: " + computerGuess);
                PrintHelper.saveLine("Result: " + bullCounterG + " bull and " + cowCounterG + " cows");
                if (computerGuess.equals(secretCode)) {
                    result = "Computer Wins! :(";
                    break outerloop;

                }
                counter++;
            }

            PrintHelper.saveLine(result + "\n" + "Play Again? Y/N");
            String playAgain = PrintHelper.keyboardSave().toLowerCase();
            if (playAgain.equals("y")) {
                start();
            }else{
                wouldYouLikeToPrint();
                System.exit(0);
            }

        }
        public void wouldYouLikeToPrint(){
            PrintHelper.saveLine("Would you like to save your answers to a file?");
            String toPrint = PrintHelper.keyboardSave();
            if (toPrint.equals("yes")) {
                File myFile = new File("input1.txt");

                try (PrintWriter out = new PrintWriter("filename.txt")) {
                    out.println(PrintHelper.getPrint());
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
//                try (PrintWriter writer = new PrintWriter (new FileWriter(myFile))) {
//                    PrintStream fileStream = new PrintStream("input1.txt");
//                    System.setOut(fileStream);
//                    }
//                }catch (IOException){
//                    PrintHelper.saveLine("You beat the system");
//                }
                PrintHelper.saveLine("Done!");

            }

        }

        //Gets Computers guess and compares, prints bull or cow depending, only runs if user doesn't go.
        public static void main (String[]args){
            GameMaster ex = new GameMaster();
            ex.start();
        }
    }
