package ictgradschool.industry.io.ex05;

import ictgradschool.Keyboard;

public class Players {
    protected String getSecretCodeOrGuess(){
        String secretCode = "";
        while(secretCode.length() != 4) {
            try {
                secretCode = PrintHelper.keyboardSave();
                Integer.parseInt(secretCode);
                if ((Integer.parseInt(secretCode) > 10000) || (Integer.parseInt(secretCode) < 1000))
                    throw new Exception("Test");
            } catch (NumberFormatException e) {
                PrintHelper.saveLine("Use numbers!!");
            } catch (Exception e) {
                PrintHelper.saveLine("The number must be four digits!!");
            }
        }
        return secretCode;
    }
    protected String getComputerCode(){
        String computerCode = String.valueOf((int)((Math.random()*9000)+1000));
        return computerCode;
    }
}

