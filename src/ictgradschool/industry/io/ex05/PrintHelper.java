package ictgradschool.industry.io.ex05;
import ictgradschool.Keyboard;

public class PrintHelper {
    static String  printString = "";
    public static void saveLine(String s){
        System.out.println(s);
        printString += s+ "\n";
    }
    public static String keyboardSave(){
        String s = Keyboard.readInput();
        printString += s+"\n";
        return s;
    }
    public static String getPrint(){
        return printString;
    }
}
