package ictgradschool.industry.io.ex02;

import ictgradschool.Keyboard;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class MyReader {

    public void start() {
        System.out.print("Enter a file name: ");
        String fileName = Keyboard.readInput();
        String s;
        try (BufferedReader bR =  new BufferedReader(new FileReader(fileName+".txt"))){
            while ((s = bR.readLine()) != null) {
//                s = bR.readLine();
                System.out.println(s);
            }
        }catch (IOException e){
            System.out.println("You win");
        }
        // TODO Prompt the user for a file name, then read and print out all the text in that file.
        // TODO Use a BufferedReader.
    }

    public static void main(String[] args) {
        new MyReader().start();
    }
}
