package ictgradschool.industry.io.ex02;

import ictgradschool.Keyboard;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class MyScanner {

    public void start() {
        System.out.print("Enter a file name: ");
        String fileName = Keyboard.readInput();
        String s;
        try (Scanner scanner =  new Scanner(new FileReader(fileName+".txt"))){
            while (scanner.hasNextLine()) {
                System.out.println(scanner.nextLine());
            }
        }catch (IOException e){
            System.out.println("You win");
        }
        // TODO Prompt the user for a file name, then read and print out all the text in that file.
        // TODO Use a Scanner.
    }

    public static void main(String[] args) {
        new MyScanner().start();
    }
}
