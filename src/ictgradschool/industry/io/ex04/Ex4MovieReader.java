package ictgradschool.industry.io.ex04;

import ictgradschool.industry.io.ex03.Movie;
import ictgradschool.industry.io.ex03.MovieReader;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

/**
 * Created by anhyd on 20/03/2017.
 */
public class Ex4MovieReader extends MovieReader {

    @Override
    protected Movie[] loadMovies(String fileName) {
        Movie[] movieArray = null;
        int i = 0;
        try (Scanner scanner =  new Scanner(new FileReader(new File(fileName+".txt")))) {
        scanner.useDelimiter(",|\\r\\n");
        movieArray = new Movie[scanner.nextInt()];
        while (scanner.hasNext()){
            String name = scanner.next();
            int year = scanner.nextInt();
            int lengthInMinutes = scanner.nextInt();
            String director = scanner.next();
            movieArray [i]=  new Movie(name,year,lengthInMinutes,director);
            i++;
        }
            // TODO Implement this with a Scanner
        }catch (IOException e){
            System.out.print("Boom");
        }
        return movieArray;
    }

    public static void main(String[] args) {
        new Ex4MovieReader().start();
    }
}
