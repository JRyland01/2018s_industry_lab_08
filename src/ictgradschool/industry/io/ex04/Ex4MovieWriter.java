package ictgradschool.industry.io.ex04;

import ictgradschool.industry.io.ex03.Movie;
import ictgradschool.industry.io.ex03.MovieWriter;

import javax.sound.midi.SysexMessage;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by anhyd on 20/03/2017.
 */
public class Ex4MovieWriter extends MovieWriter {

    @Override
    protected void saveMovies(String fileName, Movie[] films) {
        try (PrintWriter writer = new PrintWriter (new FileWriter(new File(fileName+".txt")))){
            int i = 0;
            writer.println(films.length);
            while(i<films.length){
                    String name = (films[i].getName());
                    int year = (films[i].getYear());
                    int lengthInMinutes = (films[i].getLengthInMinutes());
                    String director = (films[i].getDirector());
//                    System.out.println(name+","+year+","+lengthInMinutes+","+director);
                    writer.println(name+","+year+","+lengthInMinutes+","+director);
                    i++;
            }
        }catch (IOException e){
            System.out.println("Failure");
        }
        // TODO Implement this with a PrintWriter

    }


    public static void main(String[] args) {
        new Ex4MovieWriter().start();
    }

}
